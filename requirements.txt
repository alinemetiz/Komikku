beautifulsoup4
brotli
cloudscraper
dateparser >= 1.1.4
emoji
keyring >= 21.6.0
lxml
natsort
Pillow
pure-protobuf
PyGObject
python_magic
rarfile
Unidecode
